debug=0
for i in "$@"; do
  case $i in
  --debug)
    debug=1
    shift
    ;;
  esac
done

apt update
apt -y install curl jq grep

curl -o /usr/bin/nomadctlx -O https://repo.icts.kuleuven.be/artifactory/icts-p-lnx-generic-local/go/nomadctlx/latest/nomadctlx
chmod +x /usr/bin/nomadctlx

nomadctlx --deploykey $ELSSCHOT_DEPLOY_KEY job status "$NOMAD_JOB_NAME" > old_status.txt
OLD_DEPLOY_ID=$(grep -oP "^ID\s*=\s*\K.*" old_status.txt | tail -1)
echo "Old deployment ID = ${OLD_DEPLOY_ID}";

nomadctlx \
--deploykey $ELSSCHOT_DEPLOY_KEY \
--secret env.DATABASE_URL \
job export "$NOMAD_JOB_NAME" > old_job.json \

cat old_job.json \
| jq '.image |= "'${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}'"' \
> new_job.json

if [[ $debug -eq 1 ]]; then
  echo "deploying with log level debug"
  nomadctlx --deploykey $ELSSCHOT_DEPLOY_KEY job deploy new_job.json --log-level debug
else
  echo "deploying without debug"
  nomadctlx --deploykey $ELSSCHOT_DEPLOY_KEY job deploy new_job.json
fi

echo "Deployment finished"

echo -n "New Deployment ID = "; grep -oP "^ID\s*=\s*\K.*" new_status.txt | tail -1
