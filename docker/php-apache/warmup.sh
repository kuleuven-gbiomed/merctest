#!/usr/bin/env bash

set -eo pipefail

php /var/www/html/bin/console doctrine:migrations:migrate --no-interaction

php /var/www/html/bin/console cache:clear
chown -R ${APPLICATION_USER}:${APPLICATION_USER} /var/www/html/var
