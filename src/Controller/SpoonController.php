<?php

namespace App\Controller;

use App\Entity\Spoon;
use App\Form\SpoonFormType;
use App\Repository\SpoonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Turbo\TurboBundle;

class SpoonController extends AbstractController
{
    #[Route('/spoon', name: 'app_spoon')]
    public function index(SpoonRepository $spoonRepository, Request $request): Response
    {
        $spoons = $spoonRepository->findBy([], ['id' => 'ASC']);


        return $this->render('spoon/index.html.twig', [
            'controller_name' => 'SpoonController',
            'spoons' => $spoons,
        ]);
    }

    #[Route('/spoon/new', name: 'app_spoon_new')]
    public function new(Request$request, SpoonRepository$spoonRepository): Response
    {
        $form = $this->createForm(SpoonFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $spoon = $form->getData();
            $spoonRepository->save($spoon, true);
//            $this->addFlash('success', 'success');
            return $this->render('spoon/form.html.twig', ['form' => $form]);
        }

        return $this->render('spoon/new.html.twig', [
            'controller_name' => 'SpoonController',
            'form' => $form,
        ]);
    }

    #[Route('/spoon/remove/{id}', 'app_spoon_remove')]
    public function remove(Spoon $spoon, SpoonRepository $spoonRepository): Response
    {
        $spoonRepository->remove($spoon, true);

        return $this->redirectToRoute('app_spoon');
    }

    #[Route('/spoon/update/{id}')]
    public function update(Spoon $spoon, SpoonRepository $spoonRepository, Request $request): Response
    {
        $form = $this->createForm(SpoonFormType::class, $spoon);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $spoon = $form->getData();
            $spoonRepository->save($spoon, true);
//            $this->addFlash('success', 'success');
            return $this->redirectToRoute('app_spoon_update', ['id' => $spoon->getId()]);
//            $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
        }

        return $this->render('spoon/update.html.twig', [
            'controller_name' => 'SpoonController',
            'form' => $form->createView(),
        ]);
    }

    #[Route('/spoon/view/{id}')]
    public function view(Spoon $spoon, SpoonRepository $spoonRepository, Request $request): Response
    {
        return $this->render('spoon/view.html.twig',
        [
            'spoon' => $spoon,
        ]);
    }
}
