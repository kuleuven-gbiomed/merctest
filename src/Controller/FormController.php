<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\TestForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends AbstractController
{
    #[Route('/form', name: 'form')]
    public function testForm(Request $request): Response
    {
        $form = $this->createForm(TestForm::class, null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // save...

            return $this->redirectToRoute('form');
        }

        $response = new Response(null, $form->isSubmitted() ? 422 : 200);

        return $this->render('testform.html.twig', [
            'form' => $form,
        ], $response);
    }
}
